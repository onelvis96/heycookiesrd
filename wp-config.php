<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'heycookiessite' );

/** MySQL database username */
define( 'DB_USER', 'heycookiessite' );

/** MySQL database password */
define( 'DB_PASSWORD', 'heycookiessite' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'fo(ZMM^[iW4g;=Hz;#a53xOt7G*@9,?B8q3*H>-&<>ashLyM9O2:jb,35Q|%uJUY' );
define( 'SECURE_AUTH_KEY',  '1Rl3g!Va8%?SyDoLy3]FGsA*T<D|Mz3U4GM8G1N%#y7Cnxp_3o;:`f3gdudq!ncx' );
define( 'LOGGED_IN_KEY',    '/uk+DdkPHY9I~U1Vb[CS>z(.uIbdM:Vv_!zoa:9Decq&Tr)z`yw2%{c-Y apw<Lx' );
define( 'NONCE_KEY',        't73sOS1S#Xry! T>rALF)EG&2qW4cesu8a1J3M*S5U|<@re$!_u#{aw1=KfJ~bCE' );
define( 'AUTH_SALT',        'v3he)UVt:VZ<zzdQhe{I:7C#fvX|VH2H/o`qDR@A43lei YVg3.PAl=<3iyL4>53' );
define( 'SECURE_AUTH_SALT', 'R^[BOCAy(wFeiDG_?%(F*zQ*N<}4b{63ym95[wh/BkNlt2GBH**E`A4#PrJXQzR$' );
define( 'LOGGED_IN_SALT',   '5!ut5Zj1K^j/l=y2vb6{y:Ae/7UDr6``.#q;qjg$`V#9&VIo)T:U zfGg=VINut`' );
define( 'NONCE_SALT',       '06Z<Cn1X^Xk-!+)FcXv$AFfyGn>2J?vI0GPa>2.:z7eRJ#+uIaaid6s!%qt@z10J' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
