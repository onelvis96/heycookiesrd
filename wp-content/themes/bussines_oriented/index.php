<?php
get_header();
?>


<div class="container-fluid mt-7">
    <div class="row mb-6">
        <section class="col-lg-6">
            <h3 class="tm-title-gray mb-4">Nuevas entradas</h3>
            <hr class="mb-5 tm-hr">
            <?php while (have_posts()) {
                the_post();

            ?>

                <div class="tm-strategy-box mb-5">
                    <img src="<?php echo get_theme_file_uri('img/cookie.jpg'); ?>" alt="Image" class="img-fluid tm-strategy-img" height="200px" width="200px">
                    <div>
                        <h4 class="tm-text-primary"><?php the_title(); ?></h4>
                        <p class="tm-strategy-text"><?php the_content(); ?></p>
                    </div>
                </div>
            <?php
            }
            ?>

        </section>
    </div>
    <?php
    get_footer();
    ?>