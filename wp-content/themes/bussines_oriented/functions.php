<?php

function load_stylessheets(){


    wp_register_style('bootstrap_stylesheet', get_stylesheet_directory_uri().'/css/bootstrap.min.css',array(),false,"all");
    wp_register_style('fontawasome', get_stylesheet_directory_uri().'/fontawesome/css/all.min.css',array(),false,"all");
    wp_register_style('template_stylesheet', get_stylesheet_directory_uri().'/css/templatemo-business-oriented.css',array(),false,"all");
    wp_register_script('parallax', get_template_directory_uri().'/js/parallax.min.js',array(),false,"all" );
    

    wp_enqueue_script('parallax');
    wp_enqueue_style('style',get_stylesheet_uri(), NULL, microtime());
    wp_enqueue_style('bootstrap_stylesheet');
    wp_enqueue_style('fontawasome');
    wp_enqueue_style('template_stylesheet');
    wp_enqueue_style('google_fonts','//fonts.googleapis.com/css?family=Open+Sans');
}

add_action("wp_enqueue_scripts","load_stylessheets");